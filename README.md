![Blimp.mx](http://blimp.mx/firmas/blimp.png)

# Componentes de Blimp.mx para el reto Alerta MX #

Este repositorio hace referencia a manera de submódulos a los componentes de Blimp.mx para el reto Alerta MX

1. [Agrupador de feeds CAP y notificador (Componente de servidor)](https://bitbucket.org/blimpmx/alertamx-server)
2. [Cliente iOS](https://bitbucket.org/blimpmx/alertamx-ios)
3. [Generador de feeds CAP (Componente de servidor)](https://bitbucket.org/blimpmx/capgenerator)

**Las instrucciones de instalación se encuentran en el repositorio de cada componente.**

Para descargar el código de **todos los submódulos**:

`git clone --recursive https://bitbucket.org/blimpmx/alertamx.git`

### Demostración ###
El video con la demostración del servicio se puede encontrar en: *https://www.youtube.com/watch?v=486754Kd9Ds*

### Más información  ###
Más información en http://blimp.mx/alerta/alerta.pdf

## Modulos ##

### Agrupador de feeds CAP y notificador ###
[Repositorio](https://bitbucket.org/blimpmx/alertamx-server)

Es un sistema donde se dan de alta feeds CAP para ser procesados y notificados a los clientes.

### Cliente iOS ###
[Repositorio](https://bitbucket.org/blimpmx/alertamx-ios)

Código iOS

###Generador de feeds CAP ###
[Repositorio](https://bitbucket.org/blimpmx/capgenerator)

Genera un feed CAP y es utilizado para realizar pruebas.
Se conecta a un agrupador de feeds y recibe notificaciones CAP.

## Contacto ##

* Rodolfo Cartas rodolfo@blimp.mx

## Licencia ##
Copyright 2014 CARTAS DIAZ FLORES GURRIA LEAL MARQUEZ y ASOCIADOS SC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.